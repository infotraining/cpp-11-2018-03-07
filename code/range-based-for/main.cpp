#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>
#include <list>

using namespace std;

TEST_CASE("range-based-for")
{
   SECTION("works with std containers")
   {
       vector<int> vec = { 1, 2, 3, 4 };

       for(int item : vec)
       {
           cout << "item of vec: " << item << endl;
       }

       SECTION("is interpreted as")
       {
            for(auto it = vec.begin(); it != vec.end(); ++it)
            {
                int item = *it;
                cout << "item of vec: " << item << endl;
            }
       }

       cout << "\n";

       for(int& item : vec)
       {
           item = item * item;
       }

       SECTION("is interpreted as")
       {
            for(auto it = vec.begin(); it != vec.end(); ++it)
            {
                int& item = *it;
                item = item * item;
            }
       }

       for(int item : vec)
       {
           cout << "item of vec: " << item << endl;
       }

       cout << "\n";
   }

   SECTION("works with static arrays")
   {
       int tab[10] = { 1, 2, 3, 4, 5 };

       for(const auto& item : tab)
           cout << "item of tab:" << item << endl;

       SECTION("is interpreted as")
       {
           cout << "\n";

           for(auto it = begin(tab); it != end(tab); ++it)
           {
               const auto& item = *it;
               cout << "item of tab:" << item << endl;
           }
       }

       cout << "\n";
   }

   SECTION("works initializer list")
   {
       for(const auto& item : { "one", "two", "three" })
       {
           cout << "item of initializer_list: " << item << endl;
       }

       cout << "\n";
   }

   SECTION("can be used with auto")
   {
       vector<string> words = { "one", "two", "three" };

       SECTION("WARINING - Inefficient version")
       {
           for(auto word : words)
              cout << word << endl;
       }

       cout << "\n";

       SECTION("Efficient version")
       {
           for(const auto& word : words)
              cout << word << endl;
       }
   }

   cout << "\n";
}

template <typename Container>
void print_items(const Container& container)
{
    for(const auto& item : container)
        cout << item << " ";
    cout << endl;
}

void print_items(initializer_list<int> lst)
{
    for(const auto& item : lst)
        cout << item << " ";
    cout << endl;
}

class Gadget
{
    int id_;
    string name_;
public:
    Gadget(int id, string name) : id_{id}, name_{name}
    {}

    void play()
    {
        cout << "Playing " << name_ << endl;
    }
};

TEST_CASE("initializer_list")
{
    initializer_list<int> init_lst1 = { 1, 2, 3 };

    print_items(init_lst1);

    auto init_lst2 = { 1.0, 2.0, 3.1, 4.1 }; // initializer_list<double>

    print_items(init_lst2);

    SECTION("is used in init of std containers")
    {
        vector<int> vec = { 1, 2, 3, 4, 5 }; // call of vector<int>(initializer_list<int>)

        vec.insert(vec.begin() + 3, { 6, 7, 8, 9 });

        print_items(vec);
    }
}
