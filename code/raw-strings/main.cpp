#include <iostream>
#include <string>

using namespace std;

int main()
{
    string old_path = "c:\nasz katalog\twoj katalog\backspace";

    cout << old_path << endl;

    cout << "\n-----------------------" << endl;

    string path = R"(c:\nasz katalog\twoj katalog\backspace)";

    cout << path << endl;

    cout << "\n-----------------------" << endl;

    string multiline =
R"(Line1
Line2
Line3)";

    cout << multiline << endl;

    cout << "\n-----------------------" << endl;

    string custom_delim = R"abc(cytat: "(test)")abc";

    cout << custom_delim << endl;
}
