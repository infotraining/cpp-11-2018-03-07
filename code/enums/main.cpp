#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <vector>

using namespace std;

enum Coffee : uint64_t
{
    espresso = 1,
    cappucino,
    lura = numeric_limits<uint64_t>::max()
};

TEST_CASE("extensions for enums")
{
    Coffee c = lura;

    SECTION("buggy code")
    {
        int value = c;

        cout << value << endl;
    }

    SECTION("safe code")
    {
        underlying_type<Coffee>::type value = c;

        cout << value << endl;
    }
}

enum class EngineType : uint8_t
{
    diesel,
    tdi,
    petrol,
    lpg,
    hybrid,
    etanol
};

TEST_CASE("scoped enums")
{
    EngineType engine = EngineType::lpg;

    int value = static_cast<int>(engine);

    value++;

    engine = static_cast<EngineType>(value);

    cout << "Engine: " << static_cast<int>(engine) << endl;
}

struct Aggregate
{
    int a;
    int b[10];
    double c;
};

class Gadget
{
    int id_;
    string name_;

public:
    Gadget(int id, string name)
        : id_{id}
        , name_{name}
    {
    }

    void play()
    {
        cout << "Playing " << name_ << endl;
    }
};

double get_number()
{
    return 3.14;
}

TEST_CASE("universal init syntax")
{
    // simple types
    int x{};
    int* ptr{};
    int y{7};

    // it should be an error
    int number{get_number()};

    // static arrays
    int tab[4] = {1, 2, 3, 4};

    // aggregates
    Aggregate agg1 = {1, {1, 2, 3, 4}, 3.14};

    // objects
    Gadget g1 = {1, "ipad"};

    // std containers
    const vector<int> vec1{1, 2, 3, 4};
    vector<int> vec2 = {6, 7, 8, 9};

    const map<int, string> dict = {{1, "one"}, {2, "two"}, {3, "three"}};

    SECTION("tricky example")
    {
        SECTION("()")
        {
            vector<int> vec(10, 1);

            REQUIRE(vec.size() == 10);
            REQUIRE(all_of(vec.begin(), vec.end(), [](int x) { return x == 1; }));
        }

        SECTION("{}")
        {
            vector<int> vec{10, 1};

            REQUIRE(vec.size() == 2);
            REQUIRE((vec[0] == 10 && vec[1] == 1));
        }
    }
}

TEST_CASE("auto bug in standard")
{
    int x{2};

    SECTION("C++11")
    {
        auto y1{3}; // initializer_list<int>
        auto y2 = {1}; // initializer_list<int>
    }

    SECTION("C++17")
    {
        auto y1{3}; // int
        auto y2 = {3}; // initializer_list<int>
    }
}

template <typename Key>
using Dictionary = map<Key, string>;

TEST_CASE("aliases")
{
    SECTION("for types")
    {
        using Id = int; // typedef int Id;

        Id id = 665;
    }

    SECTION("for templates")
    {
        Dictionary<int> dict1    = {{1, "one"}, {2, "two"}};
        Dictionary<string> dict2 = {{"one", "jeden"}, {"two", "dwa"}};
    }
}
