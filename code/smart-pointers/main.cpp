#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

class Bitmap
{
    char* image_;
    size_t size_;
public:
    Bitmap(const string& path) : image_{new char[path.length()]}, size_{path.length()}
    {
        cout << "Bitmap ctor(" << path << ")" << endl;
        copy(begin(path), end(path), image_);
    }

    // copy constructor
    Bitmap(const Bitmap& source) : image_{new char[source.size_]}, size_{source.size_}
    {
        copy(source.image_, source.image_ + size_, image_);

        cout << "Bitmap copy ctor(";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << ")" << endl;
    }

    // copy assignment operator
    Bitmap& operator=(const Bitmap& source)
    {
        if (this != &source)
        {
            char* temp_image = new char[source.size_];
            copy(source.image_, source.image_ + size_, temp_image);

            delete[] image_;
            image_ = temp_image;
            size_ = source.size_;
        }

        cout << "Bitmap copy =(";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << ")" << endl;

        return *this;
    }

    // move constructor
    Bitmap(Bitmap&& source) noexcept : image_{source.image_}, size_{source.size_}
    {
        source.image_ = nullptr;

        cout << "Bitmap move ctor(";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << ")" << endl;
    }

    // move assignment
    Bitmap& operator=(Bitmap&& source) noexcept
    {
        if (this != &source)
        {
            delete[] image_;

            image_ = move(source.image_);
            source.image_ = nullptr;
            size_ = move(source.size_);
        }

        cout << "Bitmap move =(";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << ")" << endl;

        return *this;
    }


    ~Bitmap()
    {
        cout << "Bitmap dtor(";

        if (image_ == nullptr)
        {
            cout << "state after move";
        }
        else
        {
            for(size_t i = 0; i < size_; ++i)
                cout << image_[i];
        }
        cout << ")" << endl;

        delete[] image_;
    }

    void draw() const
    {
        cout << "Drawing: ";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << endl;
    }
};

void foo(auto_ptr<Bitmap> ptr)
{
    ptr->draw();
}

TEST_CASE("deprecated auto_ptr")
{
   auto_ptr<Bitmap> ptr_bmp1(new Bitmap("star1"));

   ptr_bmp1->draw();
   (*ptr_bmp1).draw();

   auto_ptr<Bitmap> ptr_bmp2 = ptr_bmp1; // copy - by move

   ptr_bmp2->draw();
   assert(ptr_bmp1.get() == nullptr);

   foo(ptr_bmp2);

   //ptr_bmp2->draw(); // SEGFAULT
}


template <typename T, typename... Arg>
std::unique_ptr<T> create_object(Arg&&... arg)
{
    //cout << "Creating an object: " << arg << endl;
    return std::unique_ptr<T>(new T{forward<Arg>(arg)...});
}


TEST_CASE("create_object")
{
    auto bmp = create_object<Bitmap>("bmp");
    auto vec = create_object<vector<int>>(10, 1);
    auto str = create_object<string>();
}

void use(Bitmap* ptr)
{
    if (ptr)
        ptr->draw();
}


void use(unique_ptr<Bitmap> ptr)
{
    if (ptr)
        ptr->draw();
}

template <typename Ptr>
void use_generic(Ptr ptr)
{
    if (ptr)
        ptr->draw();
}


TEST_CASE("unique_ptr")
{
    cout << "\n";

    unique_ptr<Bitmap> ptr_bmp1 = create_object<Bitmap>("bomb1");

    ptr_bmp1->draw();
    (*ptr_bmp1).draw();

    use(ptr_bmp1.get());

    use(move(ptr_bmp1));

    use(create_object<Bitmap>("special bomb"));

    assert(ptr_bmp1.get() == nullptr);

    ptr_bmp1 = create_object<Bitmap>("bomb2");

    use_generic(ptr_bmp1.get());

    use_generic(move(ptr_bmp1));

    cout << "\nEnd of scope" << endl;
}

namespace LegacyCode
{
    int* create_table(int size)
    {
        return new int[size];
    }
}

TEST_CASE("unique_ptr with tables")
{
    const int size = 10;

    unique_ptr<int[]> safe_tab(LegacyCode::create_table(size));

    for(int i = 0; i < size; ++i)
        safe_tab[i] = i;
}

TEST_CASE("unique_ptr with vector")
{
    unique_ptr<Bitmap> ptr = create_object<Bitmap>("bomb0");

    vector<unique_ptr<Bitmap>> bitmaps;

    bitmaps.push_back(create_object<Bitmap>("bomb1"));
    bitmaps.push_back(make_unique<Bitmap>("bomb2"));
    bitmaps.push_back(create_object<Bitmap>("bomb3"));
    bitmaps.push_back(move(ptr));

    for(const auto& b : bitmaps)
    {
        b->draw();
    }
}

TEST_CASE("shared_ptr")
{
    map<string, shared_ptr<Bitmap>> bmps;

    {
        auto sp1 = make_shared<Bitmap>("star1"); // rc1 - 1
        bmps.insert(make_pair("s1", sp1)); // rc1 - 2

        {
            auto sp2 = sp1; // rc1 - 3

            assert(sp1.use_count() == 3);

            shared_ptr<Bitmap> sp3(new Bitmap("star2")); // rc2 - 1
            auto result = bmps.insert(make_pair("s1", sp2)); // rc1 - 3
            if (!result.second)
                cout << "insert failed" << endl;

            bmps.insert(make_pair("s2", sp3)); // rc2 - 2

            assert(sp1.use_count() == 3);
            assert(sp3.use_count() == 2);
        }

        assert(sp1.use_count() == 2);
        assert(bmps["s2"].use_count() == 1);
    }

    bmps.clear();

    cout << "End of scope" << endl;
}

void print()
{
    cout << endl;
}

template <typename T, typename... Ts>
void print(T arg1, Ts... args)
{
    cout << arg1 << " ";
    print(args...);
}

TEST_CASE("variadic template")
{
    print(1, 4, 4.15, "c-string");

    print(1, 4, 4.15, "c-string", string("text"));
}
