#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>
#include <boost/type_index.hpp>

using namespace std;

string full_name(const string& first_name, const string& last_name)
{
    return first_name + " " + last_name;
}

TEST_CASE("ref binding rules")
{
   int x = 10;

   SECTION("C++98")
   {
       SECTION("l-value ref can be bound with l-value")
       {
            int& ref_x = x;
       }

       SECTION("r-value can be bound with l-value ref to const")
       {
           const string& ref_text = full_name("Jan", "Kowalski");
       }
   }

   SECTION("C++11")
   {
       SECTION("r-value can be bound with r-value reference")
       {
           string&& rv_ref_text = full_name("Jan", "Kowalski");

           rv_ref_text = "new text";
       }

       SECTION("r-value reference cannot be bound to l-value")
       {
           // int&& rv_ref_x = x; // ERROR
       }
   }
}

void load_from_file(vector<int>& vec)
{
    for(size_t i = 0; i < vec.size(); ++i)
        vec[i] = static_cast<int>(i);

    //...
}


namespace LegacyCode
{
    vector<int>* create_big_data()
    {
        vector<int>* vec = new vector<int>(1000000);

        load_from_file(*vec);

        return vec;
    }

    void calc()
    {
        vector<int>* vec = create_big_data();

        //...

        delete vec;
    }
}


namespace Modern
{
    vector<int> create_big_data()
    {
        vector<int> vec(1000000);

        load_from_file(vec);

        return vec;
    }

    void calc()
    {
        vector<int> vec = create_big_data();

        //...
    }
}

class Bitmap
{
    char* image_;
    size_t size_;
public:
    Bitmap(const string& path) : image_{new char[path.length()]}, size_{path.length()}
    {
        cout << "Bitmap ctor(" << path << ")" << endl;
        copy(begin(path), end(path), image_);
    }

    // copy constructor
    Bitmap(const Bitmap& source) : image_{new char[source.size_]}, size_{source.size_}
    {
        copy(source.image_, source.image_ + size_, image_);

        cout << "Bitmap copy ctor(";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << ")" << endl;
    }

    // copy assignment operator
    Bitmap& operator=(const Bitmap& source)
    {
        if (this != &source)
        {
            char* temp_image = new char[source.size_];
            copy(source.image_, source.image_ + size_, temp_image);

            delete[] image_;
            image_ = temp_image;
            size_ = source.size_;
        }

        cout << "Bitmap copy =(";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << ")" << endl;

        return *this;
    }

    // move constructor
    Bitmap(Bitmap&& source) noexcept : image_{source.image_}, size_{source.size_}
    {
        source.image_ = nullptr;

        cout << "Bitmap move ctor(";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << ")" << endl;
    }

    // move assignment
    Bitmap& operator=(Bitmap&& source) noexcept
    {
        if (this != &source)
        {
            delete[] image_;

            image_ = move(source.image_);
            source.image_ = nullptr;
            size_ = move(source.size_);
        }

        cout << "Bitmap move =(";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << ")" << endl;

        return *this;
    }


    ~Bitmap()
    {
        cout << "Bitmap dtor(";

        if (image_ == nullptr)
        {
            cout << "state after move";
        }
        else
        {
            for(size_t i = 0; i < size_; ++i)
                cout << image_[i];
        }
        cout << ")" << endl;

        delete[] image_;
    }

    void draw() const
    {
        cout << "Drawing: ";
        for(size_t i = 0; i < size_; ++i)
            cout << image_[i];
        cout << endl;
    }
};

TEST_CASE("Bitmap - move semantics")
{
    cout << "\n-----------------------\n\n";

    Bitmap bmp1("obraz1");
    Bitmap bmp2("obraz2");

    bmp1.draw();

    Bitmap cloned_bmp = bmp1; // copy ctor

    bmp2 = bmp1; // copy assignment: operator=

    cloned_bmp.draw();
    bmp2.draw();

    cout << "\n";

    Bitmap moved_bmp = move(cloned_bmp); // move ctor
    cloned_bmp = move(bmp2); // move assignment: operator=
    cloned_bmp.draw(); // UB

    cout << "End of scope" << endl;
}


namespace OptimizedByDefault
{
    class Sprite
    {
        Bitmap bmp_;
        int x{};
        int y{};
    public:
        Sprite(const string& path) : bmp_{path}
        {}

        void render() const
        {
            bmp_.draw();
            cout << "at coordinates: " << x << ", " << y << endl;
        }
    };
}

class Sprite
{
    Bitmap bmp_;
    int x{};
    int y{};
public:
    Sprite(const string& path) : bmp_{path}
    {}

    Sprite(const Sprite&) = default;
    Sprite(Sprite&&) = default;
    Sprite& operator=(const Sprite&) = default;
    Sprite& operator=(Sprite&&) = default;

    ~Sprite()
    {
        cout << "~Sprite()" << endl;
    }

    void render() const
    {
        bmp_.draw();
        cout << "at coordinates: " << x << ", " << y << endl;
    }
};

class X
{};

TEST_CASE("moving sprite")
{
    cout << "\nSprites:\n";

    Sprite s1{"bomb1"};
    s1.render();

    Sprite cloned_s = s1;
    cloned_s.render();

    Sprite s2 = move(s1);
    s2.render();

    cout << "\nStarting a game:\n";

    vector<Sprite> game;
    //game.reserve(10);

    game.push_back(Sprite("bomb2"));
    game.emplace_back("bomb3");
    game.push_back(Sprite("bomb4"));
    for(int i = 5; i <= 16; ++i)
        game.push_back(Sprite(string("bomb") + to_string(i)));


    for(const auto& s : game)
        s.render();

    cout << "\nEnd of scope" << endl;
}

TEST_CASE("for primitive types move does nothing")
{
    int x = 10;
    int y = move(x);

    REQUIRE(x == 10);
    REQUIRE(y == 10);
}

TEST_CASE("self assignment")
{
    Bitmap bmp("bmp");

    bmp = bmp;
}


void foo_non_template(string&& arg)
{
    cout << "foo_non_template(string&&: " << arg << ")" << endl;
}

template <typename T>
void foo_template(T&& arg)
{
    cout << "foo_template<" << boost::typeindex::type_id_with_cvr<T>().pretty_name() << ">("
         << boost::typeindex::type_id_runtime(arg).pretty_name() << ": " << arg << ")" << endl;
}

TEST_CASE("ref collapsing")
{
    foo_non_template(full_name("Jan", "Kowalski"));

    string fname = "Adam";

    foo_non_template(move(fname));

    string lname = "Nowak";

    foo_template(lname); // l-value passed as argument

    foo_template(full_name("Ewa", "Kowalska")); // r-value passed as argument

    SECTION("auto&& with ref collapsing - universal reference")
    {
        auto&& text1 = full_name("Ewa", "Kowalska"); // string&&
        auto&& text2 = lname; // string&
    }
}

template <typename T, typename Arg>
T* create_object(Arg&& arg)
{
    cout << "Creating an object: " << arg << endl;
    return new T{forward<Arg>(arg)};
}


TEST_CASE("perfect forwarding")
{
    auto ptr_bmp = create_object<Bitmap>("star");
    auto ptr_sprite = create_object<Sprite>("bomb1");

    ptr_bmp->draw();
    ptr_sprite->render();

    delete ptr_bmp;
    delete ptr_sprite;
}
