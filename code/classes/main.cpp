#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

namespace Ver_1
{
    class Gadget
    {
        int id_;

    public:
        Gadget()          = default;
        virtual ~Gadget() = default;

        Gadget(int id)
            : id_{id}
        {
        }

        int id() const
        {
            return id_;
        }
    };
}

namespace Ver_2
{
    int default_id()
    {
        static int id{};

        return ++id;
    }

    class Gadget
    {
        int id_ = default_id();
        double price_ = 0.99;
        string name_{"not-set"};

    public:
        Gadget()          = default;
        virtual ~Gadget() = default;

        Gadget(int id)
            : Gadget{id, 0.99, "-"} // delegeting of constructor
        {
            name_ += name_;
            cout << "Gadget extra initialization" << endl;
        }

        Gadget(int id, double price, const string& name) : id_{id}, price_{price}, name_{name}
        {
            cout << "Gadget(" << id_ << ", " << price_ << ", " << name_ << ")" << endl;

        }

        int id() const
        {
            return id_;
        }

        string name() const
        {
            return name_;
        }

        double price() const
        {
            return price_;
        }
    };
}

namespace Cpp98
{
    class Nocopyable
    {
    private:
        Nocopyable(const Nocopyable&);
        Nocopyable& operator=(const Nocopyable&);
    public:
        Nocopyable() {}
    };
}

class Nocopyable
{
public:
    Nocopyable(const Nocopyable&) = delete;
    Nocopyable& operator=(const Nocopyable&) = delete;
    Nocopyable() = default;
};

void perform_calc(int value)
{
    cout << "Perform calculation for int" << endl;
}

void perform_calc(double) = delete;

template <typename T1, typename T2>
typename common_type<T1, T2>::type min_value(const T1& a, const T2& b)
{
    return (a < b) ? a : b;
}

TEST_CASE("default sepcial functions")
{
    using namespace Ver_1;

    Gadget g1{101};

    REQUIRE(g1.id() == 101);

    Gadget g2;
}

TEST_CASE("deleted functions")
{
    SECTION("C++98")
    {
        Cpp98::Nocopyable nc1;
        //Cpp98::Nocopyable nc2 = nc1;
    }

    SECTION("C++11")
    {
        Nocopyable nc1;
        //auto nc2 = nc1;
    }

    SECTION("deleting any function")
    {
        perform_calc(78);

        short x = 42;
        perform_calc(x);

        // perform_calc(3.14);  // error - perform_calc(double) is deleted
        // perform_calc(3.14F);
    }
}

TEST_CASE("min_value with common_type")
{
    auto result1 = min_value(3.14, 6.28F);

    static_assert(is_same<double, decltype(result1)>::value, "Error");

    auto result2 = min_value("ala", string("stefan"));

    static_assert(is_same<string, decltype(result2)>::value, "Error");
    REQUIRE(result2 == "ala");
}

TEST_CASE("init of nonstatic members")
{
    using namespace Ver_2;

    Gadget g1;

    REQUIRE(g1.id() == 1);

    Gadget g2{};

    REQUIRE(g2.id() == 2);
    REQUIRE(g2.name() == "not-set");
}

struct Aggregate
{
    int a;
    double b;
    const char* c[3];
};

struct NonAggregate
{
    int a = 2;
    double b;
    const char* c[3];
};

TEST_CASE("init of aggreagate")
{
    Aggregate agg1 = { 1, 3.14, { "a", "b", "c"} };
}

TEST_CASE("delegating contructors")
{
    Ver_2::Gadget g{665};

    cout << g.name() << endl;
}


// inheritance of constructors

template <typename T>
class IndexedSet : public std::set<T>
{
public:
    using std::set<T>::set; // inheritance of all constructors

    const T& operator[](size_t index) const
    {
        auto it = next(std::set<T>::begin(), index);

        return *it;
    }
};

TEST_CASE("Indexed set")
{
    IndexedSet<int> sorted_set = { 6, 2, 8, 1, 665 };

    REQUIRE(sorted_set[0] == 1);
    REQUIRE(sorted_set.count(665) == 1);
}

class Base
{
public:
    virtual void foo() const
    {
        cout << "Base::foo() const" << endl;
    }

    virtual void foo()
    {
        cout << "Base::foo()" << endl;
    }

    virtual ~Base() = default;
};

class Derived : public Base
{
public:
    void foo() override
    {
        cout << "Derived::foo()" << endl;
    }

    void foo() const override
    {
        cout << "Derived::foo() const" << endl;
    }
};


TEST_CASE("overriding")
{
    Derived d;
    d.foo();

    cout << "\n";

    const Base& cref_base = d;
    cref_base.foo(); // foo() const

    cout << "\n";

    Base& ref_base = d;
    ref_base.foo(); // foo()
}

struct Notinheritable final
{
    int value;
};

class Command;

class CommandHistory
{
public:
    void save(Command*);
    Command* pop_last();
};

class Command
{
public:
    virtual void execute() = 0;
    virtual void undo() = 0;
    virtual Command* clone() = 0;
    virtual ~Command() = default;
};

class UndoableCommand : public Command
{
    CommandHistory history_;
public:
    void execute() final
    {
        do_save_state();
        history_.save(clone());
        do_action();
    }

    void undo() final
    {
        auto cmd = history_.pop_last();

        do_undo();
    }
protected:
    virtual void do_save_state() = 0;
    virtual void do_action() = 0;
    virtual void do_undo() = 0;
};


template <typename T, size_t N>
struct Array
{
    T items[N];

    using iterator = T*;
    using const_iterator = const T*;

    iterator begin()
    {
        return items;
    }

    const_iterator begin() const
    {
        return items;
    }

    iterator end()
    {
        return items + N;
    }

    const_iterator end() const
    {
        return items + N;
    }

    size_t size() const
    {
        return N;
    }

    T& operator[](size_t index)
    {
        return *(items + index);
    }

    T& at(size_t index)
    {
        if (index >= N)
        {
            throw out_of_range("Index out of range");
        }

        return *(items + index);
    }
};

TEST_CASE("std::array vs std::vector")
{
    int tab[10] = { 1, 2, 3, 4 };

    SECTION("std::array is replacement for static arrays")
    {
        array<int, 10> arr = { 1, 2, 3, 4 };

        cout << arr.size() << endl;

        int* pos = find(begin(tab), end(tab), 3);

        if (pos != end(tab))
        {
            cout << "Znalazlem 3" << *pos << endl;
        }
    }

    int* dynamic_tab = new int[10];

    SECTION("std::vector is replacement for dynamic arrays")
    {
        vector<int> vec(10);
    }

    delete[] dynamic_tab;
}
