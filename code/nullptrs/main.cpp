#include <iostream>

using namespace std;

void use(int* ptr)
{
    if (ptr)
        cout << "use: " << *ptr << endl;
    else
        cout << "use: null pointer" << endl;
}

void use(long n)
{
    cout << "use of int: " << n << endl;
}

void use(nullptr_t)
{
    cout << "use of nullptr_t" << endl;
}

int main()
{
    int x = 10;
    int* ptr = nullptr;

    use(&x);
    use(ptr);
    use(nullptr);

    // int y = nullptr; // compilation error

    if (ptr)
        cout << "*ptr: " << *ptr << endl;
}
