#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <iostream>
#include <iterator>
#include <list>
#include <memory>
#include <vector>

using namespace std;

template <typename T>
int get_number(T x)
{
    return 42 * x;
}

TEST_CASE("auto")
{
    int x        = 10;
    const int cx = 10;

    SECTION("can be used with variable declarations")
    {
        auto ax1   = 10; // int
        auto ax2   = 10L; // long
        auto ax3   = x; // int
        auto ax4   = 3.14; // double
        auto ax5   = cx; // int
        auto value = get_number(42);

        const auto cax1 = x; // const int
    }

    SECTION("with pointer")
    {
        auto ptr1   = &x; // int*
        auto ptr2   = &cx; // const int*
        auto* aptr1 = &x; // int*
    }

    int& ref_x        = x;
    const int& cref_x = x;

    SECTION("case 3")
    {
        SECTION("ref and const, volatile are stripped")
        {
            auto ax1 = ref_x; // int
            auto ax2 = cref_x; // int
            auto ax3 = cx; // int
        }

        SECTION("arrays and functions decay to pointers")
        {
            int tab[10];

            auto atab1 = tab; // int*
            auto fun   = get_number<int>; // int (*)(int) - decay to function pointer
        }
    }

    SECTION("case 1")
    {
        SECTION("ref and const, volatile are preserved")
        {
            auto& arefx1 = x; // int&

            auto& arefx2 = ref_x; // int&
            auto& arefx3 = cref_x; // const int&
        }

        SECTION("array and functions do not decay")
        {
            int tab[10];

            auto& ref_tab1 = tab; // int (&)[10]
            auto& fun      = get_number<int>; // int (&)(int) - reference to function
        }
    }
}

TEST_CASE("use cases for auto")
{
    vector<int> vec = {1, 2, 3};

    SECTION("iteration over container")
    {
        SECTION("C++98")
        {
            for (vector<int>::const_iterator it = vec.begin(); it != vec.end(); ++it)
                cout << *it << " ";
            cout << endl;
        }

        SECTION("C++11")
        {
            for (auto it = vec.cbegin(); it != vec.cend(); ++it)
                cout << *it << " ";
            cout << endl;
        }
    }

    SECTION("smart pointers")
    {
        SECTION("C++98")
        {
            int* ptr = new int(6);

            cout << *ptr << endl;

            delete ptr;
        }

        SECTION("C++11")
        {
            auto ptr = make_shared<int>(6);

            cout << *ptr << endl;
        }
    }
}

TEST_CASE("auto initialization syntax")
{
    int x = 10;

    SECTION("direct init")
    {
        auto ax(x); // int
    }

    SECTION("copy init syntax")
    {
        auto ax = x; // int
    }
}

TEST_CASE("decltype")
{
    map<int, string> dict1 = {{1, "one"}};

    decltype(dict1) dict2;

    decltype(dict1[2]) item = dict1[1];

    REQUIRE(dict1.size() == 1);

    auto dict3 = dict1;

    int value1 = 3;

    vector<decltype(value1)> vec = {value1, value1, value1};
}

template <typename T>
auto multiply(const T& a, const T& b) -> decltype(a * b)
{
    return a * b;
}

template <typename Container>
auto get_n(Container& container, size_t index) -> decltype(container[index])
{
    return container[index];
}

#if __cplusplus >= 201402L

namespace Cpp14
{
    template <typename Container>
    decltype(auto) get_n(Container& container, size_t index)
    {
        return container[index];
    }
}

#endif

TEST_CASE("decltype(auto)")
{
    vector<int> vec = {1, 2, 3};

    get_n(vec, 1) = 10;

    REQUIRE(vec[1] == 10);
}

