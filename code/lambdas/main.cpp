#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cassert>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <vector>

using namespace std;

// closure class
class Lambda_734519273579
{
public:
    void operator()() const
    {
        cout << "Lambda" << endl;
    }
};

template <typename Iterator, typename Function>
void for_each_n(Iterator start, size_t n, Function f)
{
    auto it = start;

    for (size_t i = 0; i < n; ++i, ++it)
    {
        f(*it);
    }
}

TEST_CASE("lambda")
{
    auto l = []() { cout << "Lambda" << endl; };

    l();
    l();

    //    SECTION("is iterpreted as")
    //    {
    //        auto l = Lambda_734519273579{};

    //        l();
    //        l();
    //    }

    SECTION("deduces return type")
    {
        auto multiply = [](int a, int b) { return a * b; };

        int result = multiply(6, 8);

        cout << "result: " << result << endl;
    }

    SECTION("in c++11 for complex lambda use ->")
    {
        auto describe = [](int x) -> string {
            cout << "Called describe: " << x << endl;
            if (x % 2 == 0)
                return "even";
            else
                return string("odd");
        };

        REQUIRE(describe(2) == "even");
        REQUIRE(describe(3) == "odd");
    }

    SECTION("using lambda as function parameters")
    {
        vector<int> vec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        for_each_n(vec.begin(), 5, [](int x) { cout << "item: " << x << endl; });

        vector<int> evens;

        copy_if(vec.begin(), vec.end(), back_inserter(evens), [](int x) { return x % 2 == 0; });

        for_each(evens.begin(), evens.end(), [](int x) { cout << "even: " << x << endl; });
    }
}

void foo(int x)
{
    cout << "foo(" << x << ")" << endl;
}

struct Foo
{
    void operator()(int x)
    {
        cout << "Foo::operator()(" << x << ")" << endl;
    }
};

TEST_CASE("storing lambda")
{
    SECTION("auto")
    {
        auto l = [](int x) { cout << x << endl; };
        l(4);
    }

    SECTION("non-capture lambda can be converted do function pointer")
    {
        void (*ptr_f)(int);

        ptr_f = [](int x) { cout << x << endl; };
    }

    SECTION("std::function")
    {
        function<void(int)> f;

        f = &foo;
        f(13);

        f = Foo{};
        f(14);

        f = [](int x) { cout << "Lambda: " << x << endl; };
        f(15);
    }
}

// closure class
class Lambda_73451922342423473579
{
public:
    template <typename T>
    void operator()(T arg) const
    {
        cout << "arg: " << arg << endl;
    }
};

TEST_CASE("lambda in C++14")
{
    auto l = [](auto arg) { cout << "arg: " << arg << endl; };

    l(10); // arg: int
    l(string("string")); // arg: string
}

// closure class
class Lambda_7367357657468
{
    const int threshold_;

public:
    Lambda_7367357657468(int t)
        : threshold_{t}
    {
    }

    bool operator()(int x) const
    {
        return x > threshold_;
    }
};

TEST_CASE("capturing varaibles")
{
    vector<int> vec = {245, 1, 345, 6, 655, 235, 12, 34, 6, 67};

    SECTION("capture by value")
    {
        int threshold = 100;

        auto predicate = [threshold](int x) { return x > threshold; }; // capture threshold by value

        threshold = 1000;

        auto no_gt_than_threshold = count_if(vec.begin(), vec.end(), predicate);

        cout << "no_gt_than_threshold: " << no_gt_than_threshold << endl;
    }

    SECTION("capture by reference")
    {
        int sum{};

        for_each_n(vec.begin(), 5, [&sum](int x) { sum += x; });

        cout << "sum of first 5: " << sum << endl;
    }

    SECTION("capture by value and by ref")
    {
        int threshold = 100;
        int sum{};

        auto no_gt_than_threshold = count_if(vec.begin(), vec.end(),
                                             [threshold, &sum](int x) {
                                                 if (x > threshold)
                                                 {
                                                     sum += x;
                                                     return true;
                                                 }
                                                 return false;
                                             });

        cout << "no_gt_than_threshold: " << no_gt_than_threshold << endl;
        cout << "sum_gt_than_threshold: " << sum << endl;
    }
}


function<int()> create_generator(int seed)
{
    return [seed]() mutable { return ++seed; };
}

TEST_CASE("generator")
{
    auto gen1 = create_generator(100);

    cout << gen1() << " " << gen1() << " " << gen1() << endl;

    auto gen2 = create_generator(665);

    cout << gen2() << " " << gen2() << " " << gen2() << endl;
}
