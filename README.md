# README #

### Energy efficiency ###

* http://greenlab.di.uminho.pt/wp-content/uploads/2017/09/paperSLE.pdf

### C++ books ###

* https://www.safaribooksonline.com/library/view/the-c-standard/9780132978286/
* https://www.safaribooksonline.com/library/view/discovering-modern-c/9780134383682/
* https://www.safaribooksonline.com/library/view/the-c-programming/9780133522884/
* https://www.safaribooksonline.com/library/view/tour-of-c/9780133549041/

### C++ Core Guidelines ###

* https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md